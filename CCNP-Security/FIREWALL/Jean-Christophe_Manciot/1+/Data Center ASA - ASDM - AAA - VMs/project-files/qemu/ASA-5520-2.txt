ASA Version 8.4(2)
!
hostname ASA-5520-2
domain-name Data-Center-ASAs
enable password Asa-5520
passwd Asa-5520
names
dns-guard
!
interface GigabitEthernet0
 nameif management
 security-level 100
 ip address 192.168.127.253 255.255.255.0
!
interface GigabitEthernet1
 nameif DMZ-1
 security-level 50
 ip address 192.168.12.253 255.255.255.0
!
interface GigabitEthernet2
 nameif DMZ-2
 security-level 50
 ip address 192.168.22.253 255.255.255.0
!
interface GigabitEthernet3
 nameif ASAs
 security-level 50
 ip address 192.168.120.253 255.255.255.0
!
interface GigabitEthernet4
 nameif outside
 security-level 0
 ip address dhcp setroute
!
ftp mode passive
clock timezone CEST 1
clock summer-time CEDT recurring last Sun Mar 2:00 last Sun Oct 3:00
dns domain-lookup management
dns domain-lookup DMZ-1
dns domain-lookup DMZ-2
dns server-group DefaultDNS
 name-server 192.168.1.201
 domain-name Data-Center-ASAs
same-security-traffic permit inter-interface
same-security-traffic permit intra-interface
object network Internal-Network-PAT
 subnet 192.168.0.0 255.255.0.0
access-list ASAs_access_out extended permit tcp any any eq tacacs
access-list ASAs_access_out extended permit udp any any eq tacacs
access-list ASAs_access_out extended permit udp any any eq radius
access-list management_access_out extended permit tcp any any eq tacacs
access-list management_access_out extended permit udp any any eq tacacs
access-list management_access_out extended permit udp any any eq radius
access-list outside_access_in extended permit icmp any any time-exceeded inactive
access-list outside_access_in extended permit icmp any any unreachable inactive
pager lines 24
logging enable
logging timestamp
logging list OSPF_Notifications level notifications class ospf
logging buffer-size 100000
logging asdm-buffer-size 512
logging console errors
logging monitor errors
logging buffered errors
logging history errors
logging asdm warnings
logging mail critical
logging from-address actionmystique@gmail.com
logging recipient-address manciot.jeanchristophe@gmail.com level alerts
logging device-id hostname
logging flash-bufferwrap
logging class ospf asdm notifications
logging message 711004 level notifications
logging message 710004 level notifications
mtu management 1500
mtu DMZ-1 1500
mtu DMZ-2 1500
mtu ASAs 1500
mtu outside 1500
no failover
icmp unreachable rate-limit 1 burst-size 1
asdm image disk0:/asdm-647.bin
no asdm history enable
arp timeout 14400
access-group management_access_out out interface management
access-group ASAs_access_out out interface ASAs
!
router ospf 2
 network 192.168.12.0 255.255.255.0 area 0
 network 192.168.22.0 255.255.255.0 area 0
 network 192.168.120.0 255.255.255.0 area 0
 network 192.168.127.0 255.255.255.0 area 0
 area 0
 timers spf 1 5
 timers lsa-group-pacing 10
 log-adj-changes
 redistribute connected subnets
 default-information originate metric 1
!
timeout xlate 3:00:00
timeout conn 1:00:00 half-closed 0:10:00 udp 0:02:00 icmp 0:00:02
timeout sunrpc 0:10:00 h323 0:05:00 h225 1:00:00 mgcp 0:05:00 mgcp-pat 0:05:00
timeout sip 0:30:00 sip_media 0:02:00 sip-invite 0:03:00 sip-disconnect 0:02:00
timeout sip-provisional-media 0:02:00 uauth 0:05:00 absolute
timeout tcp-proxy-reassembly 0:01:00
timeout floating-conn 0:00:00
dynamic-access-policy-record DfltAccessPolicy
user-identity default-domain LOCAL
aaa authentication ssh console LOCAL
aaa authentication enable console LOCAL
aaa authentication http console LOCAL
aaa authorization command LOCAL
aaa local authentication attempts max-fail 3
http server enable
http 192.168.0.0 255.255.0.0 management
snmp-server host ASAs 192.168.56.251 community ***** version 2c
no snmp-server location
no snmp-server contact
snmp-server community *****
snmp-server enable traps snmp authentication linkup linkdown coldstart warmstart
snmp-server enable traps syslog
snmp-server enable traps cpu threshold rising
crypto ca trustpoint ASA-5520-2-Self-Signed-Certificate
 enrollment self
 subject-name CN=ASA-5520-2.Data-Center-ASAs
 keypair RSA-key-for-Self-Signed-Certificate
 proxy-ldc-issuer
 crl configure
crypto ca certificate chain ASA-5520-2-Self-Signed-Certificate
 certificate 15be4051
    30820280 308201e9 a0030201 02020415 be405130 0d06092a 864886f7 0d010105
    05003052 31243022 06035504 03131b41 53412d35 3532302d 322e4461 74612d43
    656e7465 722d4153 4173312a 30280609 2a864886 f70d0109 02161b41 53412d35
    3532302d 322e4461 74612d43 656e7465 722d4153 4173301e 170d3133 30333133
    31383038 33355a17 0d323330 33313131 38303833 355a3052 31243022 06035504
    03131b41 53412d35 3532302d 322e4461 74612d43 656e7465 722d4153 4173312a
    30280609 2a864886 f70d0109 02161b41 53412d35 3532302d 322e4461 74612d43
    656e7465 722d4153 41733081 9f300d06 092a8648 86f70d01 01010500 03818d00
    30818902 818100cc ba6d9bc5 6b41ee02 0940997b 9c814127 86a69a65 fcfe08e2
    b5fc1094 f3fa126c bec7a3ed c15c2fe8 53099dc9 f2de9b08 2ddbe614 412afab2
    cb64cac3 eb38a71c ee49e70b 34471605 d9c82976 6f024b4a 7b4b3276 0aefda87
    5858d9a7 7ce259ec 2c76057d 88e548f8 3ee3b1f4 59f6eeb8 c2f62164 e679b23e
    be0ed267 fc4fd302 03010001 a3633061 300f0603 551d1301 01ff0405 30030101
    ff300e06 03551d0f 0101ff04 04030201 86301f06 03551d23 04183016 8014a40b
    0831ac09 583fea2f f225ac14 6b8985d8 d075301d 0603551d 0e041604 14a40b08
    31ac0958 3fea2ff2 25ac146b 8985d8d0 75300d06 092a8648 86f70d01 01050500
    03818100 2a570e76 0b28363d dfe00479 0bad5393 7468f37e 82955634 e9ac8ab2
    fbc78417 06807c2c 78689bac 983ca048 104903b8 debf7971 6469e433 2a3e5a8e
    ab06d037 3785a44a a04ed13d c00cd6f7 e990c431 64d6595b e4e966ff 9c357bdc
    5587201c 8d4ff48e 50abfa01 b91ef771 1df942e4 f2f15935 fe35bdd0 8367a00f
c7936723
  quit
telnet timeout 5
ssh timeout 5
console timeout 0
management-access management
dhcp-client broadcast-flag
dhcp-client client-id interface outside
dhcpd dns 8.8.8.8 8.8.4.4
dhcpd lease 86400
dhcpd ping_timeout 750
!
dhcpd domain DMZ-1 interface DMZ-1
!
dhcprelay server 192.168.12.201 DMZ-2
dhcprelay server 192.168.22.202 DMZ-1
dhcprelay timeout 60
threat-detection basic-threat
threat-detection scanning-threat
threat-detection statistics
threat-detection statistics tcp-intercept rate-interval 30 burst-rate 400 average-rate 200
ntp server 192.43.244.18 prefer
ntp server 83.212.108.67 prefer
ntp server 192.93.2.20 prefer
ssl trust-point ASA-5520-2-Self-Signed-Certificate management
webvpn
group-policy DfltGrpPolicy attributes
 dns-server value 8.8.8.8
 default-domain value Data-Center-ASAs
username actionmystique password Asa-5520 privilege 15
tunnel-group DefaultWEBVPNGroup webvpn-attributes
 authentication certificate
!
class-map global-class
 match default-inspection-traffic
!
!
policy-map global-policy
 class global-class
  inspect icmp
  inspect icmp error
!
service-policy global-policy global
privilege cmd level 3 mode exec command perfmon
privilege cmd level 3 mode exec command ping
privilege cmd level 3 mode exec command who
privilege cmd level 3 mode exec command logging
privilege cmd level 3 mode exec command failover
privilege cmd level 3 mode exec command vpn-sessiondb
privilege cmd level 3 mode exec command packet-tracer
privilege show level 5 mode exec command import
privilege show level 5 mode exec command running-config
privilege show level 3 mode exec command reload
privilege show level 3 mode exec command mode
privilege show level 3 mode exec command firewall
privilege show level 3 mode exec command asp
privilege show level 3 mode exec command cpu
privilege show level 3 mode exec command interface
privilege show level 3 mode exec command clock
privilege show level 3 mode exec command dns-hosts
privilege show level 3 mode exec command access-list
privilege show level 3 mode exec command logging
privilege show level 3 mode exec command vlan
privilege show level 3 mode exec command ip
privilege show level 3 mode exec command ipv6
privilege show level 3 mode exec command failover
privilege show level 3 mode exec command asdm
privilege show level 3 mode exec command arp
privilege show level 3 mode exec command route
privilege show level 3 mode exec command ospf
privilege show level 3 mode exec command aaa-server
privilege show level 3 mode exec command aaa
privilege show level 3 mode exec command eigrp
privilege show level 3 mode exec command crypto
privilege show level 3 mode exec command ssh
privilege show level 3 mode exec command vpn-sessiondb
privilege show level 3 mode exec command vpn
privilege show level 3 mode exec command dhcpd
privilege show level 3 mode exec command blocks
privilege show level 3 mode exec command wccp
privilege show level 3 mode exec command dynamic-filter
privilege show level 3 mode exec command webvpn
privilege show level 3 mode exec command service-policy
privilege show level 3 mode exec command uauth
privilege show level 3 mode exec command compression
privilege show level 3 mode configure command interface
privilege show level 3 mode configure command clock
privilege show level 3 mode configure command access-list
privilege show level 3 mode configure command logging
privilege show level 3 mode configure command ip
privilege show level 3 mode configure command failover
privilege show level 5 mode configure command asdm
privilege show level 3 mode configure command arp
privilege show level 3 mode configure command route
privilege show level 3 mode configure command aaa-server
privilege show level 3 mode configure command aaa
privilege show level 3 mode configure command crypto
privilege show level 3 mode configure command ssh
privilege show level 3 mode configure command dhcpd
privilege show level 5 mode configure command privilege
privilege clear level 3 mode exec command dns-hosts
privilege clear level 3 mode exec command logging
privilege clear level 3 mode exec command arp
privilege clear level 3 mode exec command aaa-server
privilege clear level 3 mode exec command crypto
privilege clear level 3 mode exec command dynamic-filter
privilege cmd level 3 mode configure command failover
privilege clear level 3 mode configure command logging
privilege clear level 3 mode configure command arp
privilege clear level 3 mode configure command crypto
privilege clear level 3 mode configure command aaa-server
prompt hostname context
no call-home reporting anonymous
call-home
 profile CiscoTAC-1
  no active
  destination address http https://tools.cisco.com/its/service/oddce/services/DDCEService
  destination address email callhome@cisco.com
  destination transport-method http
  subscribe-to-alert-group diagnostic
  subscribe-to-alert-group environment
  subscribe-to-alert-group inventory periodic monthly
  subscribe-to-alert-group configuration periodic monthly
  subscribe-to-alert-group telemetry periodic daily
hpm topN enable
crashinfo save disable
Cryptochecksum:c5c1daa4439a3f4583352ebdc12c60a0
: end