version 12.2
service timestamps debug uptime
service timestamps log uptime
no service password-encryption
!
hostname CellModeATM
!
boot-start-marker
boot-end-marker
!
!
ip subnet-zero
ip cef
!
!
!
no mpls traffic-eng auto-bw timers frequency 0
call rsvp-sync
!
!
!         
!
!
!
!
!
interface Loopback0
 ip address 150.100.100.254 255.255.255.255
 ip router isis 
!
interface FastEthernet0/0
 no ip address
 shutdown
 duplex half
 no clns route-cache
!
interface ATM1/0
 no ip address
 no atm enable-ilmi-trap
!
interface ATM1/0.1 mpls
 ip address 150.1.101.254 255.255.255.0
 ip router isis 
 no atm enable-ilmi-trap
 mpls label protocol tdp
 mpls ip
 tag-switching atm control-vc 1 64
 mpls atm control-vc 1 64
!
interface ATM2/0
 no ip address
 no atm enable-ilmi-trap
!
interface ATM2/0.1 mpls
 ip address 150.1.102.254 255.255.255.0
 ip router isis 
 no atm enable-ilmi-trap
 mpls label protocol tdp
 mpls ip
!
interface ATM3/0
 no ip address
 no atm enable-ilmi-trap
!
interface ATM3/0.1 mpls
 ip address 150.1.109.254 255.255.255.0
 ip router isis 
 no atm enable-ilmi-trap
 mpls label protocol tdp
 mpls ip
!
router ospf 1
 network 0.0.0.0 255.255.255.255 area 0
!
router isis 
 net 49.9999.9999.9999.9999.00
router ospf 1
 log-adjacency-changes
 network 0.0.0.0 255.255.255.255 area 0
!
router isis 
 net 49.9999.9999.9999.9999.00
!
ip classless
!         
no ip http server
!
!
!
!
!
!
control-plane
!
!
dial-peer cor custom
!
!
!
!
line con 0
 stopbits 1
line aux 0
line vty 0 4
!
!
end