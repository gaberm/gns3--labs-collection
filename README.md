<a href="https://gitlab.com/jean-christophe-manciot/gns3--labs-collection/raw/master/README.pdf" rel="Labs"><p align="center"><img alt="Labs" src="https://gitlab.com/jean-christophe-manciot/gns3--labs-collection/raw/master/Labs.png"/></p></a>
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">**A Collection of GNS3 CCNP-X Labs**</span> by <a xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName" rel="cc:attributionURL" href="https://plus.google.com/u/0/+jeanchristopheManciot-IT/posts">**Jean-Christophe Manciot**</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">**Creative Commons Attribution 4.0 International License**</a>.

## Table of Contents

- [GNS3 Labs Collection Repository Contents And Notes](#gns3-labs-collection-repository-contents-and-notes)
- [Screenshots](#screenshots)
- [Tracking And Filing Issues](#tracking-and-filing-issues)

## GNS3 Labs Collection Repository Contents And Notes

Cf. **README.pdf** at **https://gitlab.com/jean-christophe-manciot/gns3--labs-collection/blob/master/README.pdf**

## Screenshots

Cf. some **screenshots** at **https://gitlab.com/jean-christophe-manciot/gns3--labs-collection/blob/master/A%20Collection%20of%20GNS3%20self-made%20Labs.pdf**

## Tracking And Filing Issues

This can be done [here](https://gitlab.com/jean-christophe-manciot/gns3--labs-collection/issues).
